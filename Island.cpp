
/*
 *
 * Template application
 * 21.10.2016
 * C. Ingels <contact@ingels.me>
 * Tested with Urho3D 1.6 release
 *
 *
 */

#include <Urho3D/Urho3DAll.h>
#include "Island.h"
#include "Forest.h"


Island::Island(Context* context) : Application(context)
{
    context_ = context;

    mRotationAngle = 0.0f;
    mCamYaw = 0.0f;
    mCamPitch = 0.0f;
    mSpeed = 10.0f;
    mDeferredInit = true;
    mCharacterInWater= false;
    mSharkSpeed = 6.5f;
}

void Island::Setup()
{
#ifdef __DEBUG__
    engineParameters_["FullScreen"] = false;
    engineParameters_["WindowWidth"] = 1920;
    engineParameters_["WindowHeight"] = 1024;
	engineParameters_["LogLevel"] = LOG_DEBUG;
#else
    engineParameters_["FullScreen"] = true;
    engineParameters_["WindowWidth"] = 1920;
    engineParameters_["WindowHeight"] = 1080;
	engineParameters_["LogLevel"] = LOG_INFO;
#endif
    engineParameters_["WindowResizable"] = true;
	engineParameters_["Headless"] = false;
	engineParameters_["Sound"] = true;
	engineParameters_["LogName"] = "Island";
	engineParameters_["BorderLess"] = false;

}

void Island::Start()
{
    Input* input = GetSubsystem<Input>();
	input->SetMouseVisible(false);
	input->SetMouseGrabbed(true);

    Graphics* graphics = GetSubsystem<Graphics>();
    graphics->SetWindowTitle("++ Island ++");

    CreateScene();

    ConnectHandlers();
}

void Island::Stop()
{

}


void Island::CreateScene()
{
    /* Get pointer to the resources subsystem */
    ResourceCache* cache = GetSubsystem<ResourceCache>();

    /* Create a new scene */
    scene_ = new Scene(context_);

#ifdef __DEBUG__
    scene_->CreateComponent<DebugRenderer>();
#endif // __DEBUG__

    /* Register the Forest object */
    Forest::RegisterObject(context_);

    /* Disable dynamic instancing. This can be error prone on some graphic cards. */
    GetSubsystem<Renderer>()->SetDynamicInstancing(false);

    /* Create a scene octree */
    // scene_->CreateComponent<Octree>();

    /* Load the main scene */
    SharedPtr<File> sceneFile = cache->GetFile("./Data/Scenes/Island.xml");
    if(scene_->LoadXML(*sceneFile) == false)
    {
        URHO3D_LOGDEBUGF("Resource %s not found!", "Island.xml");
        exit(-1);
    }

    /* Create the physic world */
    mPhysicWorld = scene_->CreateComponent<PhysicsWorld>();
    mPhysicWorld->SetGravity(Vector3(0.0, -9.81, 0.0));


    Node* sunNode = scene_->CreateChild("Sun");
    sunNode->SetPosition(Vector3(0.0, 200.0, -9.0));
    Light* sunLight = sunNode->CreateComponent<Light>();
    sunLight->SetLightType(LightType::LIGHT_POINT);
    sunLight->SetCastShadows(true);
    sunLight->SetRange(500.0);
    sunLight->SetBrightness(0.85);
    sunLight->SetShadowBias(BiasParameters(0.00025, 0.5));
    sunLight->SetShadowCascade(CascadeParameters(1.0f, 5.0f, 10.0f, 10.0f, 0.8f));

    /* Create main character */
    {
        /* Create the main character */
        mCharacterNode = scene_->CreateChild("Character");
        mCharacterNode->SetPosition(Vector3(0.0, 23.0, 0.0));

        mCharacterModel = mCharacterNode->CreateComponent<AnimatedModel>();
        mCharacterModel->SetModel(cache->GetResource<Model>("Models/Kachujin/Kachujin.mdl"));
        mCharacterModel->SetMaterial(cache->GetResource<Material>("Models/Kachujin/Materials/Kachujin.xml"));
        mCharacterModel->SetCastShadows(false);
        mCharacterModel->SetOccluder(true);
        // mCharacterModel->SetShadowDistance(2.0);

        mCharacterBody = mCharacterNode->CreateComponent<RigidBody>();
        mCharacterBody->SetAngularFactor(Vector3(0,0,0));
        mCharacterBody->SetUseGravity(true);
        mCharacterBody->SetMass(60.0f);
        mCharacterBody->SetFriction(1.0);
        mCharacterBody->SetCcdRadius(10.0);
        mCharacterBody->SetCcdMotionThreshold(10.0);
        mCharacterBody->SetLinearDamping(0);
        mCharacterBody->SetAngularDamping(0);
        mCharacterBody->SetRestitution(0);

        mCharacterShape = mCharacterNode->CreateComponent<CollisionShape>();
        mCharacterShape->SetCapsule(0.75, 1.95, Vector3(0, 0.90, 0));
        mCharacterShape->SetMargin(0.05);

        Animation *walkAnimation = cache->GetResource<Animation>("Models/Kachujin/Kachujin_Walk.ani");
        AnimationState *state = mCharacterModel->AddAnimationState(walkAnimation);
        if(state)
        {
            state->SetWeight(1.0f);
            state->SetLooped(true);
            state->SetTime(0);
        }
    }

    /* Create a shark */
    {
        mSharkNode = scene_->CreateChild("Shark");
        mSharkNode->SetPosition(Vector3(-33.2794, 7.0, 175.393));
        mSharkNode->Rotate(Quaternion(90.0f, Vector3(0,1,0)));
        mSharkModel = mSharkNode->CreateComponent<AnimatedModel>();
        mSharkModel->SetModel(cache->GetResource<Model>("Models/Shark.mdl"));
        mSharkModel->ApplyMaterialList();
        mSharkModel->SetCastShadows(false);
        mSharkModel->SetOccluder(false);

        Animation *sharkAnimation = cache->GetResource<Animation>("Models/ArmatureAction.ani");
        AnimationState *state = mSharkModel->AddAnimationState(sharkAnimation);
        if(state)
        {
            state->SetWeight(1.0);
            state->SetLooped(true);
            state->SetTime(0);
        }

    }

    {
        Node* palmForestNode = scene_->CreateChild("Palm forest on the beach");
        palmForestNode->SetPosition(Vector3(-173.85, 100, 23.58));
        Forest* palmForest = palmForestNode->CreateComponent<Forest>();
        palmForest->SetTreesSpacingDistance(3.0f);
        palmForest->SetAngleRange(180.0f);
        palmForest->SetAngleSector(140.0f);
        palmForest->SetRandomScaleFactor(3.0f);
        uint32_t n = palmForest->Init(scene_, "Models/PalmTree.mdl", 200, 160);
        PRINTF("Number of placed trees: %d", n);
    }

    NavigationMesh* navMesh = scene_->CreateComponent<NavigationMesh>();
    navMesh->SetTileSize(256);
    navMesh->SetAgentMaxSlope(0.05);
    // navMesh->SetAgentMaxClimb(0.0f);
    navMesh->SetAgentRadius(35.0f);
    navMesh->SetAgentHeight(10.0f);
    navMesh->SetPadding(Vector3(5.0f, 0.0f, 5.0f));
    scene_->CreateComponent<Navigable>();
    navMesh->Build();

    // Vector3 nextPos = navMesh->FindNearestPoint(Vector3(255.65, 7.0, -306.75));
    Vector3 nextPos = navMesh->GetRandomPoint();
    mCurrentPath.Clear();
    navMesh->FindPath(mCurrentPath, mSharkNode->GetPosition(), nextPos);
    PRINTF("*** Number of elements in path: %d", mCurrentPath.Size());

    /* Create a camera */
    /*
    mCamNode = scene_->CreateChild("Camera");
    mCamNode->SetPosition(Vector3(4.5f, 23.0f, -3.5f));
    */
    mCamNode = mCharacterNode->CreateChild("Camera");
    mCamNode->SetPosition(Vector3(0.0f, 2.0f, -4.15f));
    Camera* camera = mCamNode->CreateComponent<Camera>();
    camera->SetFarClip(5000.0f);

    /* Create a viewport*/
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, camera));
    GetSubsystem<Renderer>()->SetViewport(0, viewport);
}

void Island::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    using namespace KeyDown;
    int key = eventData[P_KEY].GetInt();

    switch(key)
    {
        case KEY_ESCAPE:
            engine_->Exit();
            break;
        default:
            break;
    };

}

void Island::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    Input *in = GetSubsystem<Input>();

    float timeStep = eventData[P_TIMESTEP].GetFloat();

    // Vector3 p = mCharacterNode->GetPosition();

    /* Detect when character enters water or get outside of water. Water altitude is 7.0 */
    if(!mCharacterInWater && mCharacterNode->GetPosition().y_ <= WATER_LEVEL - 1.4f)
    {
        mCharacterInWater = true;
        /* Store character position when it enters into water */
        mWaterPosition = mCharacterNode->GetPosition();
        /* Force character altitude to be at water level */
        mWaterPosition.y_ = WATER_LEVEL;

#ifdef __DEBUG__
        PRINTF("Character in water!"); DisplayPosition(mWaterPosition);
#endif // __DEBUG__

    }                   /* character is assumed to be "in" water if his altitude is water level minus 1.4 */
    else if(mCharacterInWater && mCharacterNode->GetPosition().y_ > WATER_LEVEL - 1.4f)
    {
        mCharacterInWater = false;
        mSharkSpeed = SHARK_SLOW_SPEED;
        PRINTF("Character out of water!\n");
    }



    /* Manage character orientation */
    mRotationAngle += 100.0 * timeStep;
    if(mRotationAngle >= 360.0) mRotationAngle -= 360.0;
    mCamNode->SetRotation(Quaternion(mCamPitch, 0, 0));
    mCharacterNode->SetRotation(Quaternion(0, mCamYaw, 0));

    /* Manage camera movements */
    ManageCharacterMovements(timeStep);

    /* Handle shark movement */
    SharkNavigate(timeStep);

    /* Animate character */
    AnimationState *stateCharacter = mCharacterModel->GetAnimationStates()[0];
    if(in->GetKeyDown(KEY_UP))
    {
        stateCharacter->AddTime(timeStep);

        // URHO3D_LOGDEBUGF("(%f; %f; %f)", mCharacterNode->GetPosition().x_, mCharacterNode->GetPosition().y_, mCharacterNode->GetPosition().z_);
    }
    else if(in->GetKeyDown(KEY_DOWN))
    {
        stateCharacter->AddTime(-timeStep);
    }

    /* Animate the shark queue movement */
    AnimationState *state = mSharkModel->GetAnimationStates()[0];
    state->AddTime(timeStep);

    // URHO3D_LOGDEBUGF("%f;%f;%f", mCharacterNode->GetPosition().x_, mCharacterNode->GetPosition().y_, mCharacterNode->GetPosition().z_);
}

#ifdef __DEBUG__
void Island::PostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    /*
    mCharacterShape->DrawDebugGeometry(scene_->GetComponent<DebugRenderer>(), true);
    mPhysicWorld->DrawDebugGeometry(true);
    */

    // scene_->GetComponent<NavigationMesh>()->DrawDebugGeometry(true);
}

void Island::DisplayPosition(Vector3 position)
{
    Urho3D::Log::WriteRaw(Urho3D::ToString("Position: (%f, %f, %f)\n", position.x_, position.y_, position.z_));
}
#endif

void Island::HandleMouseMove(StringHash eventType, VariantMap& eventData)
{
    using namespace MouseMove;

    int dx = eventData[P_DX].GetInt();
    int dy = eventData[P_DY].GetInt();

    mCamYaw += 0.1f * dx;
    mCamPitch += 0.1f * dy;
}

void Island::ConnectHandlers()
{
    SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(Island, HandleKeyDown));
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Island, HandleUpdate));
    SubscribeToEvent(E_MOUSEMOVE, URHO3D_HANDLER(Island, HandleMouseMove));
#ifdef __DEBUG__
    SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(Island, PostRenderUpdate));
#endif
}

void Island::ManageCameraMovements(float timeStep)
{
    Input *in = GetSubsystem<Input>();


    if(in->GetKeyDown(KEY_UP))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= 30.0f;
        mCamNode->Translate(Vector3::FORWARD * dummySpeed * timeStep);
    }
    else if(in->GetKeyDown(KEY_DOWN))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= 30.0f;
        mCamNode->Translate(Vector3::BACK * dummySpeed * timeStep);
    }
    else if(in->GetKeyDown(KEY_RIGHT))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= 30.0f;
        mCamNode->Translate(Vector3::RIGHT * dummySpeed * timeStep);
    }
    else if(in->GetKeyDown(KEY_LEFT))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= 30.0f;
        mCamNode->Translate(Vector3::LEFT * dummySpeed * timeStep);
    }
}


void Island::ManageCharacterMovements(float timeStep)
{
    Input *in = GetSubsystem<Input>();

    if(in->GetKeyDown(KEY_UP))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= AcceleratedSpeedCoefficient;
        mCharacterNode->Translate(Vector3::FORWARD * dummySpeed * timeStep);
    }
    else if(in->GetKeyDown(KEY_DOWN))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= AcceleratedSpeedCoefficient;
        mCharacterNode->Translate(Vector3::BACK * dummySpeed * timeStep);
    }
    /*
    else if(in->GetKeyDown(KEY_RIGHT))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= 3.0f;
        mCharacterNode->Translate(Vector3::RIGHT * dummySpeed * timeStep);
    }
    else if(in->GetKeyDown(KEY_LEFT))
    {
        float dummySpeed = mSpeed;
        if(in->GetQualifierDown(QUAL_SHIFT))
            dummySpeed *= 3.0f;
        mCharacterNode->Translate(Vector3::LEFT * dummySpeed * timeStep);
    }
    */
}


void Island::SharkNavigate(float timeStep)
{

   /* Check if character is in water. */
    if(mCharacterInWater)
    {
        /* Get character position (in water, so Y may be under water level) */
        Vector3 p = mCharacterNode->GetPosition();

        /* Set character altitude to water level */
        p.y_ = WATER_LEVEL;

        // PRINTF("Character position: "); DisplayPosition(p);

        if((p - mWaterPosition).Length() >= 5.0f)
        {
            /* Character position in water is memorized */
            mWaterPosition = p;

            /* Get mesh subsystem */
            NavigationMesh* navMesh = scene_->GetComponent<NavigationMesh>();

            /* Compute nearest point on mesh close to character position */
            Vector3 newTarget = navMesh->FindNearestPoint(mWaterPosition, Vector3(10.0, 10.0, 10.0));

#ifdef __DEBUG__
            DisplayPosition(newTarget);
#endif // __DEBUG__

            /* Clear current path to character */
            mCurrentPath.Clear();

            /* Find a path to the new character position */
            navMesh->FindPath(mCurrentPath, mSharkNode->GetPosition(), newTarget, Vector3(1.0, 5.0, 1.0));

            /* Increase shark speed */
            mSharkSpeed = SHARK_HIGH_SPEED;

            PRINTF("New path (size: %d)\n", mCurrentPath.Size());
        }
    }

    if(mCurrentPath.Size() > 0)
    {
        Vector3 nextWaypoint = mCurrentPath[0];
        nextWaypoint.y_ = mSharkNode->GetPosition().y_;
        float movement = mSharkSpeed * timeStep;
        float distance = (mSharkNode->GetPosition() - nextWaypoint).Length();
        if(movement > distance) movement = distance;

        mSharkNode->LookAt(nextWaypoint, Vector3::UP);
        mSharkNode->Translate(Vector3::FORWARD * movement);

        if(distance < 0.1f)
        {
            mCurrentPath.Erase(0);
        }
    }
    else
    {
        mSharkSpeed = 6.5f;
        NavigationMesh* navMesh = scene_->GetComponent<NavigationMesh>();
        Vector3 newPos = navMesh->GetRandomPoint();
        navMesh->FindPath(mCurrentPath, mSharkNode->GetPosition(), newPos);
    }
}

URHO3D_DEFINE_APPLICATION_MAIN(Island);
