
#pragma once

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>


#define PRINTF(format, ...) Urho3D::Log::Write(Urho3D::LOG_DEBUG, Urho3D::ToString(format "\n", ##__VA_ARGS__))

using namespace Urho3D;


    static const float AcceleratedSpeedCoefficient = 10.0f;
    static const float WATER_LEVEL = 8.4f;
    static const float SHARK_SLOW_SPEED = 6.5f;
    static const float SHARK_HIGH_SPEED = 14.5f;


class Island : public Application
{
    URHO3D_OBJECT(Island, Application);

    public:
        Island(Context *context);
        virtual void Setup();
	virtual void Start();
	virtual void Stop();

	void CreateScene();

        void HandleKeyDown(StringHash eventType, VariantMap& eventData);
        void HandleUpdate(StringHash eventType, VariantMap& eventData);
        void HandleMouseMove(StringHash eventType, VariantMap& eventData);

        #ifdef __DEBUG__
        void PostRenderUpdate(StringHash eventType, VariantMap& eventData);
        void DisplayPosition(Vector3 position);
        #endif

        void ConnectHandlers();

        void ManageCameraMovements(float timeStep);

        void ManageCharacterMovements(float timeStep);

        void SharkNavigate(float timeStep);

    protected:

    private:
        SharedPtr<Scene> scene_;
        SharedPtr<Context> context_;
        SharedPtr<Node> mCamNode;
        SharedPtr<PhysicsWorld> mPhysicWorld;

        SharedPtr<Node> mCharacterNode;
        SharedPtr<AnimatedModel> mCharacterModel;
        SharedPtr<RigidBody> mCharacterBody;
        SharedPtr<CollisionShape> mCharacterShape;

        SharedPtr<AnimatedModel> mSharkModel;

        float mRotationAngle;

        float mCamYaw;
        float mCamPitch;

        float mSpeed;

        bool mDeferredInit;

        PODVector<Vector3> mCurrentPath;

        SharedPtr<Node> mSharkNode;
        Vector3 mSharkPosition;
        float mSharkSpeed;

        bool mCharacterInWater;
        Vector3 mWaterPosition;
};
