#ifndef FOREST_H
#define FOREST_H


#include <Urho3D/Urho3DAll.h>

class Forest : public StaticModelGroup
{
    URHO3D_OBJECT(Forest, StaticModelGroup)

    static const uint32_t PLACEMENT_MAX_RETRIES = 25;

    public:
        Forest(Context* context);
        // Forest(Context* context, Scene* parent, String modelfile);
        virtual ~Forest();

        static void RegisterObject(Context *context);

        uint32_t Init(Scene* scene, String modelfile, float_t outerRadius, int32_t numTrees);

        void SetInnerRadius(float_t value);

        void SetRandomScaleFactor(float_t value);

        void SetOffsetScaleFactor(float_t value);

        void SetTreesSpacingDistance(float_t value);

        void SetAngleSector(float_t value);

        void SetAngleRange(float_t value);

    protected:

    private:
        SharedPtr<Scene> mScene;
        SharedPtr<Node> mNode;
        String mModelFile;

        Vector<SharedPtr<Node> > treeNodes_;

        Terrain* mTerrain;

        float_t mInnerRadius;
        float_t mOuterRadius;
        int32_t mNumTrees;
        float_t mRandomScaleFactor;
        float_t mOffsetScaleFactor;
        float_t mTreesSpacingDistance;
        float_t mAngleSector;
        float_t mAngleRange;
};

#endif // FOREST_H
