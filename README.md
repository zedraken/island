This is the README.md file. It contains quite useful information on the Island game.

<h1>Overview</h1>
The game is currently under development and is written in C++. It uses Urho3D as its 3D engine and can be built for several platform: Linux, Windows and MacOS.
For the moment, the game is built and tested on a Linux machine and is in a playable state.
This game is a good opportunity for me to discover the Urho3D engine (see <a href="https://urho3d.github.io">https://urho3d.github.io</a>) which is not really known as other engines (like Unity or Ogre3D) but is very powerful with a large community and ecosystem around it.
<br />
Find below some nice screenshots of the currently playable game. Note that the character player is taken from Kachujin, a really nice character that comes along with Urho3D source package asset. I am not clever enough to design my own characters, but I am improving that knowledge and practice!

<h1>Screenshots</h1>
<img src="http://ingels.me/~charles/img/island_screenshot_01.png" size="50%"/>
<img src="http://ingels.me/~charles/img/island_screenshot_02.png" size="50%"/>
<img src="http://ingels.me/~charles/img/island_screenshot_03.png" size="50%"/>
<img src="http://ingels.me/~charles/img/island_screenshot_04.png" size="50%"/>
<img src="http://ingels.me/~charles/img/island_screenshot_05.png" size="50%"/>
<img src="http://ingels.me/~charles/img/island_screenshot_06.png" size="50%"/>
