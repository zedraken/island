# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.4

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Produce verbose output by default.
VERBOSE = 1

# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /opt/homes/charles/Projets/Urho3D/Island

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /opt/homes/charles/Projets/Urho3D/Island

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/cmake-gui -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /opt/homes/charles/Projets/Urho3D/Island/CMakeFiles /opt/homes/charles/Projets/Urho3D/Island/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /opt/homes/charles/Projets/Urho3D/Island/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named Island

# Build rule for target.
Island: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 Island
.PHONY : Island

# fast build rule for target.
Island/fast:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/build
.PHONY : Island/fast

Forest.o: Forest.cpp.o

.PHONY : Forest.o

# target to build an object file
Forest.cpp.o:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/Forest.cpp.o
.PHONY : Forest.cpp.o

Forest.i: Forest.cpp.i

.PHONY : Forest.i

# target to preprocess a source file
Forest.cpp.i:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/Forest.cpp.i
.PHONY : Forest.cpp.i

Forest.s: Forest.cpp.s

.PHONY : Forest.s

# target to generate assembly for a file
Forest.cpp.s:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/Forest.cpp.s
.PHONY : Forest.cpp.s

Island.o: Island.cpp.o

.PHONY : Island.o

# target to build an object file
Island.cpp.o:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/Island.cpp.o
.PHONY : Island.cpp.o

Island.i: Island.cpp.i

.PHONY : Island.i

# target to preprocess a source file
Island.cpp.i:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/Island.cpp.i
.PHONY : Island.cpp.i

Island.s: Island.cpp.s

.PHONY : Island.s

# target to generate assembly for a file
Island.cpp.s:
	$(MAKE) -f CMakeFiles/Island.dir/build.make CMakeFiles/Island.dir/Island.cpp.s
.PHONY : Island.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... Island"
	@echo "... Forest.o"
	@echo "... Forest.i"
	@echo "... Forest.s"
	@echo "... Island.o"
	@echo "... Island.i"
	@echo "... Island.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

