#include "Forest.h"

// Forest::Forest(Context* context, Scene* parent, String modelfile) :
Forest::Forest(Context* context) :
    StaticModelGroup(context)
{
    URHO3D_LOGDEBUG("In Forest class constructor…");

    mRandomScaleFactor = 2.5f;
    mOffsetScaleFactor = 1.6f;
    mTreesSpacingDistance = 5.0f;
    mInnerRadius = 0.0f;
    mAngleSector = 0.0f;
    mAngleRange = 360.0f;
}


Forest::~Forest()
{
    //dtor
}

void Forest::RegisterObject(Context* context)
{
    context->RegisterFactory<Forest>();
}

uint32_t Forest::Init(Scene* scene, String modelfile, float_t outerRadius, int32_t numTrees)
{
    Node* groupNode;

    mScene = scene;
    mOuterRadius = outerRadius;
    mNumTrees = numTrees;


    /* Search for the terrain within the scene */
    mTerrain = scene->GetComponent<Terrain>(true);
    /* If no terrain is found, then returns immediately */
    if(!mTerrain)
        return(0);

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    this->SetModel(cache->GetResource<Model>(modelfile));
    this->ApplyMaterialList();
    this->SetCastShadows(true);
    this->SetOccluder(true);

    groupNode = this->GetNode();

    SetRandomSeed(Time::GetSystemTime());

    for(int i = 0; i < numTrees; i++)
    {
        bool retry;
        uint32_t num_retries = 0;

        /* Create a new node for the each tree */
        Node* treeNode = groupNode->CreateChild("Tree" + i);

        /* Loop through each requested tree */
        do
        {
            retry = false;

            /* Randomly select an angle for angular position of the node around the central position */
            Quaternion q = Quaternion(Random(mAngleRange) - mAngleRange / 2.0f + mAngleSector, Vector3(0,1,0));
            /* Compute a random position based on the random angle and the random radius */
            Vector3 position = q * Vector3::FORWARD * (Random(outerRadius - mInnerRadius) + mInnerRadius);

            /* Set the node position (relative to the group node position) */
            treeNode->SetPosition(position);

            /* Check if that node is not too close to another registered node */
            for(int k = 0; k < this->GetNumInstanceNodes(); k++)
            {
                Vector3 p1 = treeNode->GetPosition();
                p1.y_ = 0.0f;
                Vector3 p2 = this->GetInstanceNode(k)->GetPosition();
                p2.y_ = 0.0f;
                if(p1.DistanceToPoint(p2) <= mTreesSpacingDistance)
                {
                    retry = true;
                    num_retries++;
                    continue;
                }
            }

        } while(retry == true && num_retries < PLACEMENT_MAX_RETRIES);

        /* If finding a location for a tree exceeds the limit, then it is considered that the forest density is too
           high for placing remaining trees. We can then return and abort placing remaining other trees. */
        if(num_retries >= PLACEMENT_MAX_RETRIES)
            return (this->GetNumInstanceNodes());

        /* Get the tree node absolute world position (this is to get terrain height at that position) */
        Vector3 absPosition = treeNode->GetWorldPosition();

        /* At the tree node position, get the terrain height */
        float_t height = mTerrain->GetHeight(absPosition);

        /* Replace height by the terrain height value so the tree will be on the ground */
        absPosition.y_ = height;

        /* Set the tree new position (on the ground, depending on the terrain height) */
        treeNode->SetWorldPosition(absPosition);

        /* Randomly scale the node vertically Y axis */
        treeNode->SetScale(Vector3(1, Random(mRandomScaleFactor) + mOffsetScaleFactor, 1));

        /* Apply a random rotation around Y axis (vertical) */
        treeNode->SetRotation(Quaternion(Random(360.0f), Vector3(0,1,0)));

        /* Add tree instance to the group node */
        this->AddInstanceNode(treeNode);
    }

    return(this->GetNumInstanceNodes());
}

void Forest::SetInnerRadius(float_t value)
{
    mInnerRadius = value;
}

void Forest::SetRandomScaleFactor(float_t value)
{
    mRandomScaleFactor = value;
}

void Forest::SetOffsetScaleFactor(float_t value)
{
    mOffsetScaleFactor = value;
}

void Forest::SetTreesSpacingDistance(float_t value)
{
    mTreesSpacingDistance = value;
}

void Forest::SetAngleSector(float_t value)
{
    mAngleSector = value;
}

void Forest::SetAngleRange(float_t value)
{
    mAngleRange = value;
}
